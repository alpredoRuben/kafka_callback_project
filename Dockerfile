FROM node:14

ARG DB_HOST
ARG DB_DATABASE
ARG DB_USERNAME
ARG DB_PASSWORD

ENV DB_HOST=$DB_HOST
ENV DB_DATABASE=$DB_DATABASE
ENV DB_USERNAME=$DB_USERNAME
ENV DB_PASSWORD=$DB_PASSWORD

ENV KAFKA_CLIENT_ID=$KAFKA_CLIENT_ID
ENV KAFKA_TOPIC_NAME=$KAFKA_TOPIC_NAME
ENV KAFKA_BROKER_HOST_1=$KAFKA_BROKER_HOST_1

RUN apt-get update && apt-get install git -y

RUN mkdir -p /var/www/node_modules && chown -R node:node /var/www/

WORKDIR /var/www/

COPY package*.json ./

USER node

COPY --chown=node:node . .

RUN cp .env.config .env

RUN npm install

#RUN npm run start

#RUN chmod -R 777 /var/www/cache

EXPOSE 5001

ENTRYPOINT [ "node", "app.js" ]

