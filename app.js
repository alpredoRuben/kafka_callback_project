var { SERVICE_PORT } = require('./config/constanta');
var cron = require('node-cron');
var app = require('./server');
const loadCronCallback = require('./src/executionCron');

// "*/10 * * * * *" 10 detik
// "*/2 * * * *" 2 menit
var task = cron.schedule('*/10 * * * * *', async () => {
    console.log('Cronjob Running Process');

    try {
        const response = await loadCronCallback();
        console.log('Success run cronjob');
        console.log('Response', response);
    } catch (error) {
        console.log('Failed run cronjob');
    }
});

task.start();
app.timeout = 30000;

app.listen(SERVICE_PORT, function () {
    console.log(`Service online => http://localhost:${SERVICE_PORT}`);
});
