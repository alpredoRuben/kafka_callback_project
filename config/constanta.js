require('dotenv').config();
module.exports = {
    MODE_PRODUCTION: true,
    SERVICE_PORT: process.env.PORT ,
};
