var axios = require('axios');
const { Op } = require('sequelize');
const Sequelize = require('sequelize');

var {
    BankAccountMutation,
    Company,
    BankAccount,
    sequelize,
} = require('../models');
const logger = require('./loggers');

/** GET BANK ACCOUNT */
async function getBankAccount(id) {
    return new Promise(async (resolve, reject) => {
        try {
            const { count, rows } = await BankAccount.findAndCountAll({
                where: {
                    [Op.and]: [
                        {
                            company_id: {
                                [Op.eq]: id,
                            },
                        },
                        {
                            status: {
                                [Op.eq]: 'active',
                            },
                        },
                        {
                            deleted_at: {
                                [Op.eq]: null,
                            },
                        },
                    ],
                },
            });
            resolve({ count, rows });
        } catch (err) {
            reject(err);
        }
    });
}

function convertToFormatTransaction(bankCode, data) {
    return {
        mutation_id: data.id,
        bank_code: data.bankcode == 'BCA3' ? 'BCA' : data.bankcode,
        account_number: data.accountbanknumber,
        transaction: data.transaction,
        type: data.type.toUpperCase() == 'OUTGOING' ? 'DEBIT' : 'CREDIT',
        datetrx: data.datetrx,
        amount: parseInt(data.amount),
        balance: parseInt(data.balance),
        trxid: bankCode == 'BRI' ? data.scrape_id : data.trxid,
        status: data.status,
        approved_by: data.approved_by,
        declined_by: data.declined_by,
        description: data.description,
        comment: data.comment,
        receiver_bank: data.receiver_bank,
        receiver_number: data.receiver_number,
        sender_bank: data.sender_bank,
        sender_name: data.sender_name,
        sender_number: data.sender_number,
        confirm: data.confirm,
        callback_delivered: data.callback_delivered,
        created_at: data.created_at,
        updated_at: data.updated_at,
        deleted_at: data.deleted_at,
        approved_at: data.approved_at,
        declined_at: data.declined_at,
    };
}

/** GET MUTATION BY */
async function getMutations(bank_code, no_rek, date_trx) {
    return new Promise(async (resolve, reject) => {
        try {
            var results = [];
            const mutations = await BankAccountMutation.findAll({
                attributes: [
                    ['id', 'mutation_id'],
                    [
                        Sequelize.literal(
                            "CASE bankcode WHEN 'BCA3' THEN 'BCA' ELSE bankcode END"
                        ),
                        'bank_code',
                    ],
                    'transaction',
                    [
                        Sequelize.literal(
                            "CASE type WHEN 'OUTGOING' THEN 'DEBIT' ELSE 'CREDIT' END"
                        ),
                        'type',
                    ],
                    'datetrx',
                    'amount',
                    'balance',
                    'trxid',
                    'status',
                    'approved_by',
                    'declined_by',
                    'description',
                    'comment',
                    'receiver_bank',
                    'receiver_number',
                    'sender_bank',
                    'sender_name',
                    'sender_number',
                    ['confirmed', 'confirm'],
                    'callback_delivered',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                    'approved_at',
                    'declined_at',
                    'scrape_id',
                ],
                where: {
                    [Op.and]: [
                        { bankcode: bank_code },
                        { accountbanknumber: no_rek },
                        {
                            datetrx: {
                                [Op.eq]: date_trx,
                            },
                        },
                        {
                            is_new: true,
                        },
                        {
                            callback_delivered: false,
                        },
                    ],
                },
                order: [['id', 'DESC']],
                raw: true,
            });

            if (mutations.length > 0) {
                results = mutations.map((x) => {
                    x.amount = parseInt(x.amount);
                    x.balance = parseInt(x.balance);
                    x.trxid = x.bankcode == 'BRI' ? x.scrape_id : x.trxid;
                    delete x.scrape_id;
                    return x;
                });

                const arrayId = results.map((x) => x.mutation_id);
                const multiUpdate = await BankAccountMutation.update(
                    {
                        callback_delivered: true,
                    },
                    {
                        where: {
                            id: arrayId,
                        },
                    }
                );

                logger.info(
                    `Update status callback delivered ${JSON.stringify({
                        updated: multiUpdate,
                    })}`
                );
            }

            resolve(results);
        } catch (err) {
            reject(err);
        }
    });
}

/** GET ALL COMPANY */
async function getAllCompanies() {
    return new Promise(async (resolve, reject) => {
        try {
            const companies = await Company.findAll({
                attributes: ['company_code', 'api_callback'],
                where: {
                    [Op.and]: [
                        {
                            api_callback: {
                                [Op.not]: null,
                            },
                        },
                        {
                            api_callback: {
                                [Op.not]: '',
                            },
                        },
                        {
                            secret_key: {
                                [Op.not]: null,
                            },
                        },
                    ],
                },
                include: {
                    model: BankAccount,
                    as: 'bank_accounts',
                    attributes: ['bankcode', 'accountbanknumber'],
                    where: {
                        [Op.and]: [
                            {
                                status: {
                                    [Op.eq]: 'active',
                                },
                            },
                            {
                                deleted_at: {
                                    [Op.eq]: null,
                                },
                            },
                        ],
                    },
                },
            });

            resolve(companies);
        } catch (error) {
            reject(error);
        }
    });
}

/** RESOLVING */
async function getResolving() {
    return new Promise(async (resolve, reject) => {
        try {
        } catch (err) {}
    });
}

async function rechangeDeliveredStatus(data) {
    if (data.bank_accounts.length > 0) {
        for (let i = 0; i < data.bank_accounts.length; i++) {
            const bankAccounts = data.bank_accounts[i];
            const listIdMutation = bankAccounts.mutations.map(
                (x) => x.mutation_id
            );

            try {
                const multiUpdate = await BankAccountMutation.update(
                    {
                        callback_delivered: false,
                    },
                    {
                        where: {
                            id: listIdMutation,
                        },
                    }
                );

                logger.info(
                    'Success Rechange status callback delivered to false'
                );
            } catch (error) {
                logger.error(
                    'Failed Rechange status callback delivered to false'
                );
            }
        }
    }
}

module.exports = {
    getMutations,
    getAllCompanies,
    getBankAccount,
    rechangeDeliveredStatus,
};
