function convertTimeZone(date, timeZoneString) {
    return new Date(
        (typeof date === 'string' ? new Date(date) : date).toLocaleString(
            'en-US',
            { timeZone: timeZoneString }
        )
    );
}

function getDateFormatZone() {
    return convertTimeZone(new Date(), 'Asia/Jakarta');
}

/** DAY */
function getDayNow() {
    var dateTimeNow = getDateFormatZone();
    var dateDayNow = dateTimeNow.getDate();
    if (dateDayNow < 10) {
        dateDayNow = '0' + dateDayNow;
    }
    dateDayNow = dateDayNow.toString();
    return dateDayNow;
}

/** MONTH */
function getMonthNow() {
    var dateTimeNow = getDateFormatZone();
    var dateMonthNow = dateTimeNow.getMonth() + 1;
    if (dateMonthNow < 10) {
        dateMonthNow = '0' + dateMonthNow;
    }
    dateMonthNow = dateMonthNow.toString();
    return dateMonthNow;
}

/** YEAR */
function getYearNow() {
    var dateTimeNow = getDateFormatZone();
    var dateYearNow = dateTimeNow.getFullYear();
    dateYearNow = dateYearNow.toString();
    return dateYearNow;
}

/** DATE */
function getDateNow() {
    return getDayNow() + '/' + getMonthNow() + '/' + getYearNow();
}

/** TIMESTAMP */
function getTimestampLive() {
    var dateTimeNow = getDateFormatZone();
    var dateHourNow = dateTimeNow.getHours();
    var dateMinuteNow = dateTimeNow.getMinutes();
    var dateSecondNow = dateTimeNow.getSeconds();
    if (dateHourNow < 10) {
        dateHourNow = '0' + dateHourNow;
    }
    if (dateMinuteNow < 10) {
        dateMinuteNow = '0' + dateMinuteNow;
    }
    if (dateSecondNow < 10) {
        dateSecondNow = '0' + dateSecondNow;
    }
    var currentTime =
        getYearNow() +
        '-' +
        getMonthNow() +
        '-' +
        getDayNow() +
        ' ' +
        dateHourNow.toString() +
        ':' +
        dateMinuteNow.toString() +
        ':' +
        dateSecondNow.toString();
    return currentTime;
}

module.exports = {
    getDateFormatZone,
    getDateNow,
    getDayNow,
    getMonthNow,
    getYearNow,
    convertTimeZone,
    getTimestampLive,
};
