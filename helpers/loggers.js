var path = require('path');
const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
        winston.format.label({
            label: path.basename('logs'),
        }),
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        winston.format.printf(
            (info) =>
                `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`
        )
    ),
    transports: [
        new winston.transports.Console(),
        new DailyRotateFile({
            filename: 'logs/server/%DATE%/combined.log',
            datePattern: 'DD-MMM-YYYY',
            level: 'info',
        }),
        new DailyRotateFile({
            filename: 'logs/server/%DATE%/errors.log',
            datePattern: 'DD-MMM-YYYY',
            level: 'error',
        }),
    ],
});

module.exports = logger;
