'use strict';
const { Model, Sequelize } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class BankAccount extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    BankAccount.init(
        {
            bankcode: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            bankaccountname: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            accountbanknumber: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            accountdescription: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            created_at: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: true,
                defaultValue: null,
            },
            updated_at: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: true,
                defaultValue: null,
            },
            user_id: {
                type: DataTypes.INTEGER,
                allowNull: true,
                defaultValue: null,
            },
            company_id: {
                type: DataTypes.INTEGER,
                allowNull: true,
                defaultValue: null,
            },
            deleted_at: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: true,
                defaultValue: null,
            },
            internetbanking_username: {
                type: DataTypes.TEXT,
                allowNull: true,
                defaultValue: null,
            },
            internetbanking_password: {
                type: DataTypes.TEXT,
                allowNull: true,
                defaultValue: null,
            },
            last_pull: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: true,
                defaultValue: null,
            },
            debit: {
                type: DataTypes.REAL(13, 2),
                allowNull: true,
                defaultValue: null,
            },
            credit: {
                type: DataTypes.REAL(13, 2),
                allowNull: true,
                defaultValue: null,
            },
            balance: {
                type: DataTypes.REAL(13, 2),
                allowNull: true,
                defaultValue: null,
            },
            many_error_auth: {
                type: DataTypes.INTEGER,
                allowNull: true,
                defaultValue: null,
            },
            renewal_at: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: true,
                defaultValue: null,
            },
            status: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            switch_datetime: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: true,
                defaultValue: null,
            },
        },
        {
            sequelize,
            tableName: 'bank_accounts',
            timestamps: false,
            modelName: 'BankAccount',
        }
    );

    BankAccount.associate = function (models) {
        BankAccount.belongsTo(models.Company, {
            foreignKey: 'company_id',
            as: 'company',
        });
    };
    return BankAccount;
};
