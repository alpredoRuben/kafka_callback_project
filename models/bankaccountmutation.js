'use strict';
const { Model, Sequelize } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class BankAccountMutation extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    BankAccountMutation.init(
        {
            bankcode: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            accountbanknumber: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            transaction: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            type: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            datetrx: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: true,
                defaultValue: null,
            },
            amount: {
                type: DataTypes.DOUBLE(11, 2),
                allowNull: true,
                defaultValue: null,
            },
            balance: {
                type: DataTypes.DOUBLE(11, 2),
                allowNull: true,
                defaultValue: null,
            },
            endpoint_id: {
                type: DataTypes.INTEGER,
                allowNull: true,
                defaultValue: null,
            },
            approved_by: {
                type: DataTypes.TEXT,
                allowNull: true,
                defaultValue: null,
            },
            declined_by: {
                type: DataTypes.TEXT,
                allowNull: true,
                defaultValue: null,
            },
            trxid: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            created_at: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: true,
                defaultValue: null,
            },
            updated_at: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: true,
                defaultValue: null,
            },
            description: {
                type: DataTypes.TEXT,
                allowNull: true,
                defaultValue: null,
            },
            receiver_bank: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            receiver_number: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            sender_bank: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            sender_name: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            sender_number: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            status: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: 'pending',
            },
            confirmed: {
                type: DataTypes.INTEGER,
                allowNull: true,
                defaultValue: null,
            },
            html: {
                type: DataTypes.TEXT,
                allowNull: true,
                defaultValue: null,
            },
            comment: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            comment_by: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            bank_date_status: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            deleted_at: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: true,
                defaultValue: null,
            },
            scrape_id: {
                type: DataTypes.BIGINT,
                allowNull: true,
                defaultValue: null,
            },
            is_new: {
                type: DataTypes.BOOLEAN,
                allowNull: true,
                defaultValue: false,
            },
            callback_delivered: {
                type: DataTypes.BOOLEAN,
                allowNull: true,
                defaultValue: false,
            },
        },
        {
            tableName: 'bank_account_mutations',
            sequelize,
            modelName: 'BankAccountMutation',
            timestamps: false,
        }
    );
    return BankAccountMutation;
};
