'use strict';
const { Model, Sequelize } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Company extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Company.init(
        {
            name: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            phonenumber: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            email: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            company_code: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            status: {
                type: DataTypes.INTEGER,
                allowNull: true,
                defaultValue: null,
            },
            created_at: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: true,
                defaultValue: null,
            },
            updated_at: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: true,
                defaultValue: null,
            },
            api_callback: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null,
            },
            secret_key: {
                type: DataTypes.TEXT,
                allowNull: true,
                defaultValue: null,
            },
        },
        {
            tableName: 'companies',
            timestamps: false,
            sequelize,
            modelName: 'Company',
        }
    );

    Company.associate = function (models) {
        Company.hasMany(models.BankAccount, {
            foreignKey: 'company_id',
            as: 'bank_accounts',
        });
    };
    return Company;
};
