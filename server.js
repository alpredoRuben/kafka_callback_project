var express = require('express');
var bodyParser = require('body-parser');
var errorhandler = require('errorhandler');
var app = express();

const { MODE_PRODUCTION } = require('./config/constanta');
const production = process.env.production || MODE_PRODUCTION;

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb', extended: true }));

if (production == false) {
    app.use(errorhandler());
    app.use(function (err, req, res, next) {
        console.log(err.message);
        res.status(err.status || 500).json({
            errors: {
                message: err.message,
                error: err,
            },
        });
    });
} else {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500).json({
            errors: {
                message: err.message,
                error: {},
            },
        });
    });
}

module.exports = app;
