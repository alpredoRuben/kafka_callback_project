require('dotenv').config();

const { default: axios } = require('axios');
const { Kafka } = require('kafkajs');
const logger = require('../helpers/loggers');
const {
    clientId,
    topic,
    ssl,
    brokers,
    groupId,
    fromBeginning,
} = require('./kafka-config');
const runProducer = require('./producers');
const saveToFile = require('./savetofile');

const kafka = new Kafka({
    clientId,
    brokers,
    ssl,
});

const consumer = kafka.consumer({ groupId });

const runConsumer = async () => {
    await consumer.connect();
    await consumer.subscribe({ topic, fromBeginning });
    await consumer.run({
        eachMessage: async ({ topic, partition, message, heartbeat }) => {
            const data = {
                value: message.value.toString(),
                headers: message.headers,
                offset: message.offset,
                topic,
            };

            const jsonData = JSON.parse(data.value);

            if (jsonData) {
                saveToFile(jsonData);
                var api_callback = jsonData.api_callback;

                console.log(`SEND DATA TO URL CALLBACK : ${api_callback}`);

                try {
                    const response_data = await axios.post(
                        api_callback,
                        jsonData,
                        {
                            headers: { 'Content-Type': 'application/json' },
                        }
                    );

                    console.log('RESPONSE ', response_data);

                    if (response_data) {
                        console.log(
                            `Success to send data to URL Callback ${api_callback}`
                        );

                        logger.info(
                            `Success to send data to URL Callback ${api_callback}`
                        );
                    }
                } catch (error) {
                    console.log(
                        `Failed send data to URL Callback ${api_callback}`,
                        error
                    );

                    logger.error(
                        `Failed send data to URL Callback ${api_callback}`
                    );

                    runProducer(jsonData);
                }
            }
        },
    });
};

module.exports = runConsumer;
