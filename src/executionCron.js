const getRowsData = require('./getRowsData');
const { default: axios } = require('axios');
const logger = require('../helpers/loggers');
const { rechangeDeliveredStatus } = require('../helpers/databaseHelper');

async function runProcessAgain(data) {
    try {
        const response = await axios.post(data.api_callback, data, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        if (response) {
            logger.info(
                `Success to send data again to URL Callback ${data.api_callback}`
            );
        }
    } catch (err) {
        logger.error(
            `Failed again send data to URL Callback ${data.api_callback}`
        );
        await rechangeDeliveredStatus(data);
    }
}

async function loadCronCallback() {
    try {
        const data = await getRowsData();

        console.log('Check Companies', data.length);

        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                if (data[i].bank_accounts.length > 0) {
                    try {
                        const response = await axios.post(
                            data[i].api_callback,
                            data[i],
                            {
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                            }
                        );
                        if (response) {
                            logger.info(
                                `Success to send data to URL Callback ${data[i].api_callback}`
                            );
                        }
                    } catch (err) {
                        logger.error(
                            `Failed send data to URL Callback ${data[i].api_callback}`
                        );
                        // await runProcessAgain(data[i]);
                        await rechangeDeliveredStatus(data[i]);
                    }
                }
            }
        }

        return data;
    } catch (err) {
        return err;
    }
}

module.exports = loadCronCallback;
