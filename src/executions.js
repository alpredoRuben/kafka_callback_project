const runConsumer = require('./consumers');
const runProducer = require('./producers');
const getRowsData = require('./getRowsData');

async function loadApiCallback() {
    try {
        const data = await getRowsData();

        if (data.length > 0) {
            console.log('DATA LENGTH ', data.length);

            for (let i = 0; i < data.length; i++) {
                runProducer(data[i]).catch((error) =>
                    console.log('Producer Error', error)
                );

                runConsumer().catch((error) =>
                    console.log('Consumer Error', error)
                );
            }
        }
    } catch (err) {}
}

module.exports = loadApiCallback;
