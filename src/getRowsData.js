require('dotenv').config();

var { getAllCompanies, getMutations } = require('../helpers/databaseHelper');
var {
    getDayNow,
    getMonthNow,
    getYearNow,
} = require('../helpers/datetimeHelper');

const logger = require('../helpers/loggers');
var saveToFile = require('./savetofile');

function getFormatDateNow() {
    const day_now = getDayNow();
    const month_now = getMonthNow();
    const year_now = getYearNow();
    return year_now + '-' + month_now + '-' + day_now;
}

async function execBankAccount(dataAccount) {
    const format_date = getFormatDateNow();

    try {
        var results = [];
        for (let i = 0; i < dataAccount.length; i++) {
            const mutations = await getMutations(
                dataAccount[i].bankcode,
                dataAccount[i].accountbanknumber,
                format_date
            );

            if (mutations.length > 0) {
                results.push({
                    bank_code: dataAccount[i].bankcode,
                    account_number: dataAccount[i].accountbanknumber,
                    mutations,
                    total_row: mutations.length,
                });
            }
        }

        return results;
    } catch (err) {
        return err;
    }
}

async function getRowsData() {
    try {
        const companies = await getAllCompanies();

        var results = [];

        if (companies.length > 0) {
            for (let i = 0; i < companies.length; i++) {
                var full_data = {
                    company_code: companies[i].company_code,
                    api_callback: companies[i].api_callback,
                    bank_accounts: [],
                };

                if (companies[i].bank_accounts.length > 0) {
                    const responses = await execBankAccount(
                        companies[i].bank_accounts
                    );
                    full_data.bank_accounts = responses;

                    logger.info('Success Collect Data');
                }

                results.push(full_data);
                saveToFile(full_data);
            }
        }

        return results;
    } catch (error) {
        return error;
    }
}

module.exports = getRowsData;
