require('dotenv').config();

let listBrokers = [];

if (process.env.KAFKA_BROKER_HOST_1) {
    listBrokers.push(process.env.KAFKA_BROKER_HOST_1);
}

if (process.env.KAFKA_BROKER_HOST_2) {
    listBrokers.push(process.env.KAFKA_BROKER_HOST_2);
}

if (process.env.KAFKA_BROKER_HOST_3) {
    listBrokers.push(process.env.KAFKA_BROKER_HOST_3);
}

module.exports = {
    clientId: process.env.KAFKA_CLIENT_ID,
    topic: process.env.KAFKA_TOPIC_NAME,
    ssl: false,
    brokers: listBrokers,
    fromBeginning: true,
    groupId: process.env.KAFKA_GROUP_ID,
};
