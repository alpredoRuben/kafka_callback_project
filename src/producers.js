require('dotenv').config();

const { Kafka } = require('kafkajs');
const logger = require('../helpers/loggers');
const { clientId, topic, ssl, brokers } = require('./kafka-config');

const kafka = new Kafka({
    clientId,
    brokers,
    ssl,
});

const producer = kafka.producer();

const runProducer = async (data) => {
    try {
        await producer.connect();
        logger.info('Connect To Producer');
        await producer.send({
            topic,
            messages: [
                {
                    value: JSON.stringify(data),
                },
            ],
        });
        logger.info('Success send to Consumer');
    } catch (error) {
        console.log(error);
        logger.error('Failed produce message ', JSON.stringify(error));
    }
};

module.exports = runProducer;
