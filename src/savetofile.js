const fs = require('fs');

function saveToFile(data) {
    var jsonContent = JSON.stringify(data);
    fs.writeFile('output.json', jsonContent, 'utf-8', function (err) {
        if (err) {
            console.log('An error occured while writing JSON Object to File.');
            return console.log(err);
        }

        console.log('JSON file has been saved.');
    });
}

module.exports = saveToFile;
